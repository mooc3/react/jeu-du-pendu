import '../styles/Keyboard.css';
import ALPHABET from '../datas/alphabet';
import PropTypes from 'prop-types';

const alphabet = ALPHABET.split('');

function Keyboard ({onClick, disabled}) {
  return (
    <div className="keyboard">
      {alphabet.map((letter) =>
        <button
          key={letter}
          onClick={() => onClick(letter)}
          className="keyboard-letter"
          disabled={disabled}
        >{letter}</button>
      )}
    </div>
  );
}

Keyboard.propTypes = {
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired
};

export default Keyboard;