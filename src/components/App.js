import '../styles/App.css';
import WORDS_LIST from '../datas/wordsList.js';
import {useState} from 'react';
import Keyboard from './Keyboard';
import Mask from './Mask';

/** Vérifie si la lettre est inclue dans le mot */
function isMatched (word, letter) {
  return word.includes(letter);
}
/** Vérifie si une lettre à déjà été essayé */
function isAttempted (attemptedLetter, letter) {
  return attemptedLetter.includes(letter);
}
/** Vérifie si la partie est gagnée */
function isWon (word, matchedLetter) {
  const setWord = Array.from(new Set(word))
  return setWord.length === matchedLetter.length
}
/** Retourne un nouveau mot */
function newWord () {
  return WORDS_LIST[Math.floor(Math.random() * WORDS_LIST.length)].toUpperCase().split('');
}

function App() {
  const [word, setWord] = useState(newWord());
  const [matchedLetter, setMatchedLetter] = useState([]);
  const [attemptedLetter, setAttemptedLetter] = useState([])
  const [score, setScore] = useState(0);
  const [attempt, setAttempt] = useState(0);

  /** Arbitrage de la lettre choisie */
  function handleKeyboard (letter) {
    const newMatched = [...matchedLetter, letter];
    const newAttempted = [...attemptedLetter, letter];
    const newScore = score;
    const newAttempt = attempt + 1;

    if (!isAttempted(attemptedLetter, letter)) setAttemptedLetter(newAttempted);

    if (isMatched(word, letter) && !isAttempted(attemptedLetter, letter)) {
      setMatchedLetter(newMatched);
      setScore(newScore + 2);
    } else if (!isMatched(word, letter) && !isAttempted(attemptedLetter, letter)) {
      setScore(newScore - 1);
    } else if (!isMatched(word, letter) && isAttempted(attemptedLetter, letter)) {
      setScore(newScore - 2);
    };

    setAttempt(newAttempt);
  }

  /** Réinitialise les différentes valeurs et définit un nouveau mot */
  function reloadGame () {
    setAttempt(0);
    setMatchedLetter([]);
    setAttemptedLetter([]);
    setWord(newWord());
  }

  return (
    <div className="App">
      <div className="game">
        <Mask word={word} matchedLetter={matchedLetter} />
        <Keyboard onClick={handleKeyboard} disabled={isWon(word, matchedLetter)} />
      </div>
      <div className="side">
        <div className="side-score">
          <span className="side-score--text">Score</span>
          <span className="side-score--number">{score}</span>
          <span className="side-score--text">Attempts</span>
          <span className="side-score--number">{attempt}</span>
          <button onClick={reloadGame}>Rejoué</button>
        </div>
      </div>
    </div>
  );
}

export default App;
