import '../styles/Mask.css';
import PropTypes from 'prop-types';

function Mask ({word, matchedLetter}) {
  return (
    <div className="mask">
      {word.map((letter, index) => 
        <div 
          key={index}
          className={`mask-letter${!matchedLetter.includes(letter) ? " empty" : ""}`}
        >
          {matchedLetter.includes(letter) && `${letter}`}
        </div>
      )}
    </div>
  );
}

Mask.propTypes = {
  word: PropTypes.arrayOf(PropTypes.string).isRequired,
  matchedLetter: PropTypes.arrayOf(PropTypes.string).isRequired
};

export default Mask;